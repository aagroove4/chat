pragma solidity ^0.4.16;

import './Room.sol';

contract Token{
    mapping (address => uint256) balances;

    function balanceOf(address _owner) constant public returns (uint256 balance) {
        return balances[_owner];
    }
}

contract TokenRoom is Room {

    Token token;
    uint threshold;

    function TokenRoom(address _token, uint _threshold, string _metadata) public {
        token = Token(_token);
        threshold = _threshold;
        metadata = _metadata;
    }
    function getPermissions(address user) constant public returns(bytes32){
        if(token.balanceOf(user) >= threshold) {
            return (0x1);
        }
        else{
            return (0x0);
        }
    }
}

