pragma solidity ^0.4.16;

contract Token {
    mapping (address => uint256) balances;

    function balanceOf(address _owner) constant public returns (uint256 balance) {
        return balances[_owner];
    }
}

contract MetaTokenRoom {
    string public metadata;

    function MetaTokenRoom(string _metadata) public {
        metadata = _metadata;
    }

    function getPermissions(address user, address _token, uint _threshold) constant public returns(bytes32){
        if(Token(_token).balanceOf(user) >= _threshold) {
            return (0x1);
        }
        else {
            return (0x0);
        }
    }

    function changeMetadata(string newMetadata) public {
        metadata = newMetadata;
    }
}
