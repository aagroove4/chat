import React from 'react';
import ReactMarkdown from 'react-markdown'

class RoomInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        // display explainer message for the default room
        if (!this.props.room) {
            var home = window.location
            var roomLink = "[" + this.props.config.networks.rinkeby.defaultRoom + "]" +"(" + home + "?" + this.props.config.networks.rinkeby.defaultRoom + ")"
            var message = [
                "This is an interface for smart-contract based chat rooms",
                "This is super low security. It's essentially just an arbitrary message relayer",
                "You can write rules for in smart contracts that meet this ABI",
                "\n",
                "`" + JSON.stringify(this.props.config.abi) + "`",
                "\n",
                "Go to an address and hit login, logout when you're done",
                "If you want to try an room check out: " + roomLink,
                "\n",
                "You can check out what commands are available with /help"
            ].join('\n');
            this.props.addRoomInfo({name: 'SpeakEasy', description: message })

        } else {
            // load medatadata for a specific room
            const roomAddress = this.props.room.split('/')[0]
            var roomContract = this.props.roomContract.at(roomAddress)
            roomContract.metadata().then((result) => {
                if (result[0].slice(0,2) == 'Qm') {
                    var url = "https://ipfs.infura.io:5001/api/v0/cat?arg=" + result[0];
                    fetch(url).then(function(response){
                        return(response.json())
                    }).then((object) => {
                        this.props.addRoomInfo(object)
                    })
                }
            })
        }
    }

    //TODO create a div element for this message
    render() {
        return (
            <div>
                <ReactMarkdown source={this.props.metadata.description}/>
            </div>
        )
    }
}

export default RoomInfo;
